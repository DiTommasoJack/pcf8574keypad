/*
	Pcf8574 keypad
	this class provides an interface to a keypad that is connected to
	a PCF8574 IC connected via I2C. This class handles all low 
	level communication, except the I2C must be initialised by
	the user. 
	
	This library allows you to configure the pcf8574 device however you desire.



*/


#ifndef Pcf8574Keypad_h
#define Pcf8574Keypad_h

//Arduino.h provides basic arduino functionality
#include "Arduino.h"
//Wire.h provides the i2c functionality
#include <Wire.h>




//KeyEvent
//a simple data class that holds the data for a single 
class KeyEvent{
	public:
		//key
		//a char value representing the key
		byte key;
		//event
		//a value representing if the key has been pressed or released
		//0 - released
		//1 - pressed
		byte event;
	
};


class Pcf8574Keypad{
	private:
	
		//the max number of keys that can be in any acceptable keypad
		//this is used to set the array size for the keyMap
		static const byte MAX_KEYS = 16;
	
		//const value, the maximum number of events that can be in the event buffer
		static const uint8_t MAX_KEY_EVENTS = 10;
	
		//the address of the device on the i2c
		//the address does not include the r/w bit
		byte deviceAddress;
		
		//numRows and numCols are the number of rows and columns in the keypad
		byte numRows, numCols;
		
		//rowPins and colPins contain which pin is connected to row and column respectively. 
		//the value in the ith position of rowPins should be which pin on the pcf8574 the ith row of the keypad is connected to
		//this is the same for colpins
		//the row pins should be tied to Vcc through a pull up resistor
		//for example if a keypad has 3 rows, and its rows are connected 1st row - pin 0, 2nd row - pin 4, 3rd - pin 3
		//then the row pins array should be [0, 4, 3]
		byte * rowPins = NULL;
		byte * colPins = NULL;
		
		//keyMap maps the position on the keypad to an actual key. The array passed should be only be 1-d.
		//the keys should be put in order of columns, from top to bottom, and left to right
		//if your keypad was like the one below
		//		1 2 3
		//      4 5 6
		//then your keyMap would be [1, 4, 2, 5, 3, 6]
		byte * keyMap = NULL;
		
		
		//keeps track of the previous state of each key
		//this is used to compare with the current state of the keys to see if any have changed
		//this is updated in the updateKeyStates() function using the setter function
		//1 - key is pressed
		//0 - key is not pressed
		byte prevKeyStates  [MAX_KEYS];
		
		//sets the keystate in the keyStates array
		void setPrevKeyState(byte row, byte col, byte set);
		
		//gets the key state from the array
		byte getPrevKeyState(byte row, byte col);
	
		
		
		
		//a buffer for the key events
		//this can be accessed by the function pop key events
		//the events are pushed into the array in circular order
		//events are added in the array. If an event would be put in an index beyond the array
		//it is instead put back in the first position of the buffer
		//When an event is removed, the array is instead indexed starting at the next 
		//position in the buffer
		//Events are stored in FIFO order
		KeyEvent keyEventBuffer [MAX_KEY_EVENTS]; 
		
		//keeps track of the position in the array
		//this is the position of the first event in the buffer
		int8_t keyEventPosition = 0;
		
		//keeps track of the number of events in the buffer
		int8_t numKeyEvents = 0;
		
		//pushKeyEvent
		//addes the event provided to the key event buffer
		//If the buffer is full the event is ignored
		void pushKeyEvent(KeyEvent event);
		
		
	
		//writeByte
		//the byte is what byte should be written to the pcf8574 device
		//the function writes to the address provided by the user
		void writeByte(byte);
		//readByte
		//reads the pcf8574 device
		//uses the address provided by the user
		byte readByte();
		
		//getKeyFromMap 
		//gets the corresponding key from based on the position on the keypad
		byte getKeyFromMap(byte row, byte col);
	
	public:
		//basic initalising function
		//sets the keyStates all to zero, and sets all events in the buffer to blank data
		Pcf8574Keypad(byte _deviceAddress, byte _numRows, byte _numCols, byte _rowPins [], byte _colPins [], byte _keyMap []);
		
		//updateKeyStates
		//this scans the keypad and updates the keyEvents
		//this should be called as often as possible
		void updateKeyStates();
		
		
		
		//popKeyEvent
		//removes the key event from the buffer and returns the next key event
		
		KeyEvent popKeyEvent();
		
		//getNumKeyEvents
		//gets the number of key events that are in the buffer
		//should be used before trying to pop a key event
		int8_t getNumKeyEvents();
		
	
	
};



#endif