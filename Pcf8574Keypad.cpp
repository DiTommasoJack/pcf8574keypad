
#include "Pcf8574Keypad.h"

void Pcf8574Keypad::writeByte(byte b){
  Wire.beginTransmission(deviceAddress);
  Wire.write(b);
  Wire.endTransmission();
  return;
}

byte Pcf8574Keypad::readByte(){
  Wire.requestFrom(deviceAddress, (uint8_t) 1);
  while(Wire.available() == 0){}
  return Wire.read();
}

byte Pcf8574Keypad::getKeyFromMap(byte row, byte col){

	return keyMap[row + col*numRows];

}

Pcf8574Keypad::Pcf8574Keypad(byte _deviceAddress, byte _numRows, byte _numCols, byte _rowPins [], byte _colPins [], byte _keyMap []){
	deviceAddress = _deviceAddress;
	
	numRows = _numRows;
	rowPins = _rowPins;
	
	numCols = _numCols;
	colPins = _colPins;
	
	keyMap = _keyMap;
	
	for(byte i = 0; i < MAX_KEYS; i++){
		prevKeyStates[i] = 0;
	}
	
	KeyEvent temp;
	temp.key = '0';
	temp.event = 0;
	
	for(uint8_t i = 0; i < MAX_KEY_EVENTS; i++){
		temp.event = i;
		keyEventBuffer[i] = temp;
		
	}
}

void Pcf8574Keypad::setPrevKeyState(byte row, byte col, byte set){
	prevKeyStates[row + col*numRows] = set;
	return;
}
		
byte Pcf8574Keypad::getPrevKeyState(byte row, byte col){
	return prevKeyStates[row + col*numRows];
}
		
void Pcf8574Keypad::updateKeyStates(){
	//iterate through each of the columns
	for(byte i = 0; i < numCols; i++){
		
		//set the current column output to low, so when we scan the keys
		//the respective row pin will be low if the key is pressed
		writeByte(~(1 << colPins[i]));
		byte newValue = readByte(); 
		
		//iterate over each row, and see if the key is pressed
		//the key is pressed if there is a zero in its respective
		//position in the read byte
		for(byte j = 0; j < numRows; j++){
			
			//determine the current key state by masking out the key bitset
			//the currentKeyState will be 1 if the key at row j, col i 
			//is pressed
			byte mask = ~(1 << rowPins[j]);
			byte currentKeyState = ((newValue | mask) == mask);
			
			//if the keyState is different from when it was last read
			//we will generate an event and push that to the buffer
			if(getPrevKeyState(j, i) != currentKeyState){
				KeyEvent newEvent;
				newEvent.key = getKeyFromMap(j, i);
				newEvent.event = currentKeyState;
				pushKeyEvent(newEvent);
				
				//update the keyState array to the newest value
				setPrevKeyState(j, i, currentKeyState);
				
			}
			
		}
	}
	
	return;
}

void Pcf8574Keypad::pushKeyEvent(KeyEvent event){
	
	//check if the buffer is full
	//if the buffer is full we simply ignore the newest event
	if(numKeyEvents == MAX_KEY_EVENTS){
		return;
	}
	
	//this pushes the event into the buffer at the position after the last added 
	//event. This is moduloed so it will wrap around to the beginning of the array
	keyEventBuffer[(keyEventPosition + numKeyEvents) % MAX_KEY_EVENTS] = event;
	
	//increment the number of events, because an event was added
	numKeyEvents += 1;
	
	return;
	
}
		

KeyEvent Pcf8574Keypad::popKeyEvent(){
	//save the event that needs to be returned before updating values
	KeyEvent temp = keyEventBuffer[keyEventPosition];
	
	//there will be one less event in the buffer
	numKeyEvents -= 1;
	
	//increment the position in the key event buffer, then check 
	//if it has exceeded the length of the array, if we have then 
	//go back to the first position in the buffer
	keyEventPosition += 1;
	if(keyEventPosition == MAX_KEY_EVENTS){
		keyEventPosition = 0;
	}
	
	return temp;
}

int8_t Pcf8574Keypad::getNumKeyEvents(){
	return numKeyEvents;
	
}

